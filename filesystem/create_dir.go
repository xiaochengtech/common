package filesystem

import (
	"errors"
	"os"
)

// 生成目录
func CreateDir(path string) (err error) {
	fileInfo, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			// 生成完整的目录路径
			err = os.MkdirAll(path, 0700)
		}
	} else {
		if !fileInfo.IsDir() {
			err = errors.New("该目录存在同名文件")
		}
	}
	return
}
