package filesystem

import (
	"encoding/json"
	"io/ioutil"
)

// 读取Json文件
func LoadJsonFile(filepath string, v interface{}) (err error) {
	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, v)
	return
}
