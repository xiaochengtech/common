# common

各种常用工具的集合。

### 基础(basic)

* [IndexOf(Type)](basic/find.go)：在某种类型的数组中，查找元素的位置。
* [SQLCommon](basic/sql_common.go)：SQL操作对象的公用接口，兼容`sql.DB`和`sql.Tx`。
* [UUIDString](basic/uuid.go)：生成32位(没有"-"符号)的UUID。
* [GetNanoTimeLabel](basic/time.go)：生成21位的纳秒时间值。
* [MarshalJson](basic/json.go)：JSON序列化。
* [UnmarshalJson](basic/json.go)：JSON反序列化。

### 加解密工具(encrypt)

* [AesCbcPkcs5Encrypt](encrypt/aes.go)：AES/CBC/PKCS5模式加密。
* [AesCbcPkcs5Decrypt](encrypt/aes.go)：AES/CBC/PKCS5模式解密。
* [HMAC-SHA1](encrypt/hmac_sha1.go)：HMAC-SHA1算法。
* [SHA1](encrypt/sha.go)：SHA1算法。
* [MD5](encrypt/md5.go)：MD5算法。

### 文件系统(filesystem)

* [CopyFile](filesystem/copy_file.go)：拷贝文件。
* [CreateDir](filesystem/create_dir.go)：创建目录。
* [LoadJsonFile](filesystem/load_json_file.go)：读取Json文件。
* [IsFile](filesystem/stat.go)：判断是否是文件。
* [IsDir](filesystem/stat.go)：判断是否是目录。

### 传输工具(transfer)

* [ReadBigEndian/WriteBigEndian](transfer/big_endian.go)：大端序字节流转换。
* [ReadLittleEndian/WriteLittleEndian](transfer/little_endian.go)：小端序字节流转换。
* [UtfToGbk/GbkToUtf](transfer/utf8_gbk.go)：UTF-8和GBK编码转换。
* [UtfToGb2312/Gb2312ToUtf](transfer/utf8_gb2312.go)：UTF-8和GB2312编码转换。
* [ReadConn/ReadConnMaxLength](transfer/read_from_connection.go)：从连接中读取指定长度的数据，可以选择是否判断最大长度。
* [ByteArrayNoZeroString](transfer/byte_array.go)：定长数组去掉多余的0后转字符串，先将定长数组转为切片后传入。

### 数据库工具(dbtool)

这里使用GORM框架，数据库采用MySQL，包含一些常用的工具，包括数据库连接、分页查询列表等。

#### GORM-MySQL

可以通过接口，来直接获取GORM的MySQL数据库连接：

```go
func GetGormMySQLConnection(config MySqlConfig) (db *gorm.DB, err error)
```

这里`MySqlConfig`是MySQL的配置信息，格式如下：

* `Host`：服务器主机地址。
* `Port`：服务器端口号。
* `User`：服务器登陆用户。
* `Password`：调试时数据库密码。
* `Scheme`：数据库名称。
* `MaxIdleConnection`：最大闲置连接数。
* `MaxOpenConnection`：最大打开连接数。
* `Params`：连接参数。

#### 通用事务处理

使用如下接口，进行GORM事务处理：

```go
func TransactionGORM(db *gorm.DB, f TransactionFuncGORM) (data interface{}, err error)
```

里面的`f`参数是实际事务操作的回调函数，类型是：

```go
type TransactionFuncGORM func(*gorm.DB) (interface{}, error)
```

#### 复杂查询条件

包括原子级条件项和复合条件项，其中：

* 原子级条件项：只有左值、右值、操作符，拼接成一个原子级查询语句。
* 复合条件项：包括多个子项，子项可以是原子级条件项，也可以是复合条件项，用连接符连接，可以是and或or。

其中`QuerySql`接口可以获取生成的查询语句和条件项，`Fields`接口可以获取查询条件中的所有字段列表，用于判断是否有字段不符合后台要求。

#### 判断GORM事务

判断一个GORM操作对象是否是事务：

```go
func IsGormTx(db *gorm.DB) error
```
