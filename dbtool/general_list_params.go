package dbtool

// 分页查询列表的公共参数
type ListByPageParams struct {
	Fields        []string `json:"fields"`         // 查询的字段，若为空数组，则查询全部字段
	IgnoreFields  []string `json:"ignore_fields"`  // 忽略掉哪些字段
	PageNumber    uint64   `json:"page_number"`    // 从1开始的页码
	PageSize      uint64   `json:"page_size"`      // 大于1为有效的页大小
	SortBy        string   `json:"sort_by"`        // 按哪个字段名称排序
	SortDirection bool     `json:"sort_direction"` // true为ASC，false为DESC
}
