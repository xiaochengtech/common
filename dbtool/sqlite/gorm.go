package sqlite

import (
	"github.com/jinzhu/gorm"
	_ "github.com/xeodou/go-sqlcipher"
)

// 获取GORM的SQLite数据库连接
func GormConnection(config Config) (db *gorm.DB, err error) {
	// 数据库连接
	db, err = gorm.Open("sqlite3", config.Filepath+"?_key="+config.Password)
	return
}
