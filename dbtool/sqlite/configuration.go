package sqlite

// SQLite的配置信息格式
type Config struct {
	Filepath  string `json:"Filepath"`  // 文件地址
	Password  string `json:"Password"`  // 调试时数据库密码
	DebugMode bool   `json:"DebugMode"` // 开启SQL调试模式
}
