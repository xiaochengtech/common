package dbtool

import (
	"github.com/jinzhu/gorm"
)

// 记录表名称和对应的Item项的映射关系
var TableMap = make(map[string]interface{})

// 获取表的主键列表
func GetTablePrimaryKeys(db *gorm.DB, tableName string) (keys []string) {
	item, ok := TableMap[tableName]
	if !ok {
		return
	}
	for _, field := range db.NewScope(item).GetModelStruct().PrimaryFields {
		keys = append(keys, field.DBName)
	}
	return
}
