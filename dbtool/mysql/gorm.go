package mysql

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"strings"
)

// 获取GORM的MySQL数据库连接
func GormConnection(config Config) (db *gorm.DB, err error) {
	// 参数拼接
	params, i := make([]string, len(config.Params)), 0
	for k, v := range config.Params {
		params[i] = fmt.Sprintf("%s=%s", k, v)
		i++
	}
	// 数据库连接
	dataSource := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.Scheme,
		strings.Join(params, "&"),
	)
	db, err = gorm.Open("mysql", dataSource)
	if err != nil {
		return
	}
	// 最大连接数
	if config.MaxOpenConnection > 0 {
		db.DB().SetMaxOpenConns(config.MaxOpenConnection)
	}
	// 最大闲置数
	if config.MaxIdleConnection > 0 {
		db.DB().SetMaxIdleConns(config.MaxIdleConnection)
	}
	return
}
