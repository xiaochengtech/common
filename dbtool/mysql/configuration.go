package mysql

// MySQL的配置信息格式
type Config struct {
	Host              string            `json:"host"`                // 服务器主机地址
	Port              int               `json:"port"`                // 服务器端口号
	User              string            `json:"user"`                // 服务器登陆用户
	Password          string            `json:"password"`            // 调试时数据库密码
	Scheme            string            `json:"scheme"`              // 数据库名称
	MaxIdleConnection int               `json:"max_idle_connection"` // 最大闲置连接数
	MaxOpenConnection int               `json:"max_open_connection"` // 最大打开连接数
	DebugMode         bool              `json:"debug_mode"`          // 开启SQL调试模式
	Params            map[string]string `json:"params"`              // 连接参数
}
