package dbtool

import (
	"testing"
)

func TestCondition(t *testing.T) {
	condition := Condition{
		Subs: []Condition{
			{
				Subs: []Condition{},
				Items: []ConditionItem{
					{
						Left:  "create_time",
						Right: 15,
						Op:    OpGreaterThan,
					},
					{
						Left:  "create_time",
						Right: 30,
						Op:    OpLessThanEqual,
					},
				},
				Conn: ConnAnd,
			},
			{
				Subs: []Condition{},
				Items: []ConditionItem{
					{
						Left:  "name",
						Right: "测试",
						Op:    OpEqual,
					},
					{
						Left:  "name",
						Right: "%Hello%",
						Op:    OpLike,
					},
					{
						Left:  "hello < ? and kitty > ?",
						Right: []interface{}{"123", 233},
						Op:    OpCustom,
					},
				},
				Conn: ConnOr,
			},
		},
		Items: []ConditionItem{
			{
				Left:  "status",
				Right: []uint8{1, 2, 3},
				Op:    OpIn,
			},
		},
		Conn: ConnAnd,
	}
	whereSql, whereArgs, err := condition.QuerySql()
	if err != nil {
		t.Error(err)
	}
	t.Logf("SQL语句: %s\n", whereSql)
	t.Logf("SQL值参: %+v\n", whereArgs)
	fields := condition.Fields()
	t.Logf("关联字段: %+v\n", fields)
}
