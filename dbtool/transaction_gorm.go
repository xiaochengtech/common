package dbtool

import (
	"github.com/jinzhu/gorm"
)

// 事务实际操作函数
type TransactionFuncGORM func(*gorm.DB) (interface{}, error)

// GORM事务处理
func TransactionGORM(db *gorm.DB, f TransactionFuncGORM) (data interface{}, err error) {
	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
			tx.Rollback()
		}
	}()
	data, err = f(tx)
	if err != nil {
		tx.Rollback()
		return
	}
	tx.Commit()
	return
}
