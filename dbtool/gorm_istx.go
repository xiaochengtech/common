package dbtool

import (
	"database/sql"
	"errors"
	"github.com/jinzhu/gorm"
)

func IsGormTx(db *gorm.DB) (err error) {
	if db == nil {
		err = errors.New("数据库对象为空")
		return
	}
	_, ok := db.CommonDB().(*sql.Tx)
	if !ok {
		err = errors.New("数据库对象不是事务")
		return
	}
	return
}
