package transfer

import (
	"bytes"
)

// 定长数组去掉多余的0后转字符串，先将定长数组转为切片后传入
func ByteArrayNoZeroString(arr []byte) (rst string) {
	idx := bytes.IndexByte(arr, 0)
	if idx >= 0 {
		rst = string(arr[:idx])
	} else {
		rst = string(arr)
	}
	return
}
