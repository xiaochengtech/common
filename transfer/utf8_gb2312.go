package transfer

import (
	"bytes"
	"io/ioutil"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

// UTF-8转换为GB2312
func UtfToGb2312(text []byte) (result []byte, err error) {
	var r bytes.Buffer
	writer := transform.NewWriter(&r, simplifiedchinese.HZGB2312.NewEncoder())
	if _, err = writer.Write(text); err != nil {
		return
	}
	writer.Close()
	result = r.Bytes()
	return
}

// GB2312转换为UTF-8
func Gb2312ToUtf(text []byte) (result []byte, err error) {
	reader := transform.NewReader(strings.NewReader(string(text)), simplifiedchinese.HZGB2312.NewDecoder())
	result, err = ioutil.ReadAll(reader)
	return
}
