package transfer

import (
	"bytes"
	"encoding/binary"
)

// 按小端序解析[]byte到结构体
func ReadLittleEndian(content []byte, target interface{}) (err error) {
	reader := bytes.NewReader(content)
	if err = binary.Read(reader, binary.LittleEndian, target); err != nil {
		return
	}
	return
}

// 将数据转换为小端序
func WriteLittleEndian(content interface{}) (result []byte, err error) {
	contentWriter := new(bytes.Buffer)
	if err = binary.Write(contentWriter, binary.LittleEndian, content); err != nil {
		return
	}
	result = contentWriter.Bytes()
	return
}
