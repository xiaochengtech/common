package transfer

import (
	"bytes"
	"encoding/binary"
)

// 按大端序解析[]byte到结构体
func ReadBigEndian(content []byte, target interface{}) (err error) {
	reader := bytes.NewReader(content)
	if err = binary.Read(reader, binary.BigEndian, target); err != nil {
		return
	}
	return
}

// 将数据转换为大端序
func WriteBigEndian(content interface{}) (result []byte, err error) {
	contentWriter := new(bytes.Buffer)
	if err = binary.Write(contentWriter, binary.BigEndian, content); err != nil {
		return
	}
	result = contentWriter.Bytes()
	return
}
