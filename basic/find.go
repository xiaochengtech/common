package basic

import (
	"strings"
)

func IndexOfString(arr []string, target string) int {
	for i, item := range arr {
		if strings.Compare(item, target) == 0 {
			return i
		}
	}
	return -1
}

func IndexOfNoCaseString(arr []string, target string) int {
	for i, item := range arr {
		if strings.EqualFold(item, target) {
			return i
		}
	}
	return -1
}
