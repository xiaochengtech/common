package basic

import (
	"encoding/json"
)

func MarshalJson(data interface{}) string {
	if data == nil {
		return "{}"
	}
	content, _ := json.Marshal(data)
	return string(content)
}

func UnmarshalJson(content string, data interface{}) (err error) {
	err = json.Unmarshal([]byte(content), data)
	return
}
