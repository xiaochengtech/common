package basic

import (
	"sync"
	"testing"
)

func TestGetTimeLabel(t *testing.T) {
	wg := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			t.Log(GetNanoTimeLabel())
		}()
	}
	wg.Wait()
}
