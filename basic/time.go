package basic

import (
	"fmt"
	"time"
)

// 获取唯一的时间标识(21位)
func GetNanoTimeLabel() (cur string) {
	now := time.Now()
	cur = fmt.Sprintf("%s%09d", now.Format("060102150405"), now.UnixNano()%1e9)
	return
}
