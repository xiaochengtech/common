package encrypt

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
)

// HMAC-SHA1算法
func HmacSHA1(plainText string, key string) string {
	mac := hmac.New(sha1.New, []byte(key))
	_, _ = mac.Write([]byte(plainText))
	return hex.EncodeToString(mac.Sum([]byte("")))
}
