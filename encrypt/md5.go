package encrypt

import (
	"crypto/md5"
	"encoding/hex"
)

// MD5算法
func MD5(plainText string) string {
	hash := md5.New()
	_, _ = hash.Write([]byte(plainText))
	hashSign := hash.Sum(nil)
	return hex.EncodeToString(hashSign)
}
